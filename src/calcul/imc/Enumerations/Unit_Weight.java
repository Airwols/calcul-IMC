/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calcul.imc.Enumerations;

import static calcul.imc.Enumerations.Unit_Size.values;

/**
 *
 * @author wslowikowski
 */
public enum Unit_Weight {

    KG("kg", 1), G("g", 0.001), LB("li", 0.4536);

    private final String unit;
    private final double coeffTokg;

    Unit_Weight(String _unit, double _coeffTokg) {
        this.unit = _unit;
        this.coeffTokg = _coeffTokg;
    }
    public String getUnit(){
       return unit;
   }
    public double getCoeffTokg() {
       return coeffTokg;
   }
    public static Unit_Weight getWeightFromUnit(String unit){
        for (Unit_Weight c : values()){
            if (c.unit.toUpperCase().equals(unit.toUpperCase())){
                return c;
            }
        }
        return null;
    }
    public static String toStringUnits(){
        String res = "";
        Unit_Weight[] tabUs = values();
        for (int i = 0; i<tabUs.length; i++){
            Unit_Weight us = tabUs[i];
            if(i == tabUs.length -1){
                res += us.getUnit();
            }
            else{
            res += us.getUnit() + ",";
            }
        }
        return res;
    }
}
    
    